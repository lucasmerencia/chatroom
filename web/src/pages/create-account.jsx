import React, { useState } from 'react';

import { 
    Title, 
    Panel, 
    Field, 
    Button, 
    ErrorMessage,
    LinkButton
} from '../ui';

import UserService from '../services/user-service';
import { useAuth } from '../auth-context';

const userService = new UserService();

const CreateAccount = () => { 
    const authContext = useAuth();

    const [data, setData] = useState({
        username: '',
        password: '',
        passwordConfirmation: ''
    })

    const [errorMessage, setErrorMessage] = useState('');

    function updateValue(event){
        setData({ ...data, [event.target.name]: event.target.value})
    }

   async function handleSubment(event){
        event.preventDefault();
        setErrorMessage('');
        try{
            await userService.createAccount(data);
            await authContext.login(data);
        } catch (error){
            setErrorMessage(error.message);
        }
   }

    return (
        <Panel>
            <Title>Create Account</Title>
            <form onSubmit={handleSubment}>
                { errorMessage && <ErrorMessage>{ errorMessage }</ErrorMessage> }
                <Field label="Username" name="username" value={data.username} onChange={updateValue}/>
                <Field label="Password" name="password" type="password" onChange={updateValue}/>
                <Field label="Password Confirmation" name="passwordConfirmation" type="password" onChange={updateValue}/>
                <Button>Create Account</Button>
                <LinkButton to="/login">Login</LinkButton>
            </form>
        </Panel>
    )
}

export default CreateAccount; 