import React, { useState } from 'react';
import { useHistory } from "react-router-dom";
import { 
    Title, 
    Panel, 
    Field, 
    Button, 
    ErrorMessage,
    LinkButton
} from '../ui';

import RoomService from '../services/room-service';
import { useAuth } from '../auth-context';

const roomService = new RoomService();

const CreateRoom = () => { 
    const { authToken } = useAuth();
    const history = useHistory();

    const [data, setData] = useState({
        name: ''
    })

    const [errorMessage, setErrorMessage] = useState('');

    function updateValue(event){
        setData({ ...data, [event.target.name]: event.target.value})
    }

   async function handleSubment(event){
        event.preventDefault();
        setErrorMessage('');
        try{
            await roomService.createRoom(authToken, data);
            history.push('/')
        } catch (error){
            setErrorMessage(error.message);
        }
   }

    return (
        <Panel>
            <Title>Create Room</Title>
            <form onSubmit={handleSubment}>
                { errorMessage && <ErrorMessage>{ errorMessage }</ErrorMessage> }
                <Field label="Name" name="name" value={data.username} onChange={updateValue}/>
                <Button>Create Room</Button>
                <LinkButton to="/">Back</LinkButton>
            </form>
        </Panel>
    )
}

export default CreateRoom; 