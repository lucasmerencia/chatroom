import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';

import { io } from "socket.io-client";
import styled from 'styled-components';
import { useAuth } from '../auth-context';

import { ChatRooms, Chat } from '../components';
import { LinkButton, Button } from '../ui';

import env from '../env';

const Main = styled.div`
    display: flex;
    height: 100%;
    width: 100%;
`;

const SideBar = styled.div`
    display: flex;
    flex-flow: column;
    width: 100%;
    max-width: 300px;
    background-color: #124E78;
`

const Content = styled.div`
    flex: 1;
`

const SidebarButton = styled(LinkButton)`
    border-radius: 0;
    width: 100%;
    background-color: #F4D58D;
    color: #835F0C;
    border: 1px #A87A10 solid;
    text-decoration: none;
    text-align: center;
    &:active {
        background-color: #F8E4B5
    }
    margin: 1px 0;
`;

const Blankslate = styled.div`
    flex: 1;
    height: 100%;
    display: flex;
    flex-flow: column;
    justify-content: center;
    align-items: center;
`

const Notice = styled.div`
    margin: 24px;
    font-size: 18px;
    color: #124E78;
`

const Home = () => {
    const { authToken, logout } = useAuth();
    const [channel, setChannel] = useState(null); 
    const [selectedRoom, setSelectedRoom] = useState();

    useEffect(() => {
        console.log('initializing socket')
        const socket = io(env.apiUrl);

        if(selectedRoom){
            socket.emit('access-room', { roomId: selectedRoom.id, authToken })
        }
        
        setChannel(socket);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return(
        <Main>
            <SideBar>
                <ChatRooms onSelect={(room) => {
                    if(channel && (!selectedRoom || selectedRoom.id !== room.id)){
                        setSelectedRoom(room);
                        channel.emit('access-room', { roomId: room.id, authToken })
                    }
                }}/>
                <SidebarButton to="/create-room">Create Room</SidebarButton>
                <SidebarButton onClick={() => { logout() }}>Logout</SidebarButton>
            </SideBar>
            <Content>
                { channel && selectedRoom ? 
                    <Chat room={selectedRoom} channel={channel}></Chat> : <SelectARoom/>
                }
            </Content>
        </Main>
    )
}

function SelectARoom(){
    const history = useHistory();
    return (
        <Blankslate> 
            <Notice>
                Select a room at the sidebar or create a new one:
            </Notice>
            <Button onClick={() => {
                history.push('/create-room');
                }}>Create Room</Button>
        </Blankslate>
    )
}

export default Home;