import React, { useState } from 'react';

import { 
    Title, 
    Panel, 
    Field, 
    Button, 
    ErrorMessage,
    LinkButton
} from '../ui';

import { useAuth } from '../auth-context';


const CreateAccount = () => { 
    const authContext = useAuth();

    const [data, setData] = useState({
        username: '',
        password: '',
    })

    const [errorMessage, setErrorMessage] = useState('');

    function updateValue(event){
        setData({ ...data, [event.target.name]: event.target.value})
    }

   async function handleSubment(event){
        event.preventDefault();
        setErrorMessage('');
        try{
            await authContext.login(data);
        } catch (error){
            setErrorMessage(error.message);
        }
   }

    return (
        <Panel>
            <Title>Login</Title>
            <form onSubmit={handleSubment}>
                { errorMessage && <ErrorMessage>{ errorMessage }</ErrorMessage> }
                <Field label="Username" name="username" value={data.username} onChange={updateValue}/>
                <Field label="Password" name="password" type="password" onChange={updateValue}/>
                <Button>Login</Button>
                <LinkButton to="/create-account">Create Account</LinkButton>
            </form>
        </Panel>
    )
}

export default CreateAccount; 