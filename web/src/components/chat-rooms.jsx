import React, {useEffect, useState} from 'react';
import RoomService from '../services/room-service';
import { useAuth } from '../auth-context';
import styled from 'styled-components';

const Chatrooms = styled.div`
    display: block;
    width: 100%;
    padding: 24px 0;
    flex: 1;
`;

const Room = styled.div`
    display: block;
    width: 100%;
    color: #FFFFFF;
    height: 40px;
    cursor: pointer;
    &:active {
        background-color: #345F9A;
    }
    padding: 0 24px;
`;

const roomService = new RoomService();

const ChatRooms = ({ onSelect }) => {

    const { authToken } = useAuth();

    const [rooms, setRooms] = useState();

    useEffect(() => {
        async function loadRooms(){
            const rooms = await roomService.listAll(authToken);
            setRooms(rooms);
        } 

        loadRooms();
    }, [authToken]);

    return (
        <Chatrooms>
            { rooms && rooms.map((room) => { 
                    return (
                        <Room key={room.id} onClick={() => {
                            onSelect(room)
                        }}>
                            {room.name}
                        </Room>
                    )
                }) 
            }
        </Chatrooms>
    )
}

export default ChatRooms;