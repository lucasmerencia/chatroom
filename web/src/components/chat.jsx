import React from 'react';
import styled from 'styled-components';

import { Title, TextArea, Button } from '../ui';

import { AuthContext } from '../auth-context';

import { v4 as uuid } from 'uuid';

const ChatContainer = styled.div`
    display: flex;
    height: 100%;
    flex-flow: column;
    padding: 24px;
`

const ChatTitle = styled(Title)`
    width: 100%;
    margin-bottom: 0;
    padding-bottom: 24px;
`

const Messages = styled.div`
    background-color: #FFFFFF;
    width: 100%;
    flex: 1;
    border: 1px #d6d6d6 solid;
    padding: 24px;
`

const Message = styled.form`
    display: flex;
    flex-flow: row;
    padding-top: 24px;
`

class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            messages: [],
            message: '',
        }
    }
    componentDidMount(){
        const currentUsername = this.context.username;
        this.props.channel.on('message-recieved', (message) => {
            if(this.state.messages.filter(msg => msg.id === message.id).length === 0 ){
                if(message.username === currentUsername){
                    message.username = 'You'
                }
                this.setState( { ...this.state, messages: this.state.messages.concat(message)});
            }
        })

        this.props.channel.on('previous-messages', (messages) => {  
            messages.forEach(msg => {
                if(msg.username === currentUsername){
                    msg.username = 'You'
                }
            } );
            this.setState( { ...this.state, messages: messages.concat(this.state.messages)});
        })
    }
    componentDidUpdate(prevState){
        if(this.props.room.id !== prevState.room.id){
            this.setState( { ...this.state, messages: []});
        }
    }
    updateMessage(message){
        this.setState({ ...this.state, message});
    }
    handleSubmit(){
        const channel = this.props.channel;
        const room = this.props.room;

        if(channel && this.state.message){
            const newMessage = { 
                time: new Date(), 
                message: this.state.message, 
                authToken: this.context.authToken, 
                roomId: room.id, 
                username: 'You', 
                id: uuid() 
            }
            this.setState( { ...this.state, message: ''});
            if(!newMessage.message.startsWith('/')){
                this.setState( { message: '', messages: this.state.messages.concat(newMessage)});
            }
            channel.emit('new-message', newMessage);
        }
    }   
    formatTime(timeString){
        const date = new Date(timeString);
        return `${date.getHours().toString().padStart(2, '0')}:${date.getMinutes().toString().padStart(2, '0')}:${date.getSeconds().toString().padStart(2, '0')}`;
    }
    render(){
        const room = this.props.room;

        return (
            <ChatContainer>
                <ChatTitle>{room.name}</ChatTitle>
                <Messages>
                    { this.state.messages.map( message =>{
                        return (
                            <div key={message.id}>
                                <i>{ this.formatTime(message.time) }</i> - <b>{message.username}</b>: {message.message}
                            </div>
                        )
                    })}
                </Messages>
                <Message onSubmit={(event) => {
                        event.preventDefault();
                        this.handleSubmit();
                    }}>
                    <TextArea value={this.state.message} onChange={(event) => {
                        this.updateMessage(event.target.value)
                    }} onKeyUp={(event)=>{
                        if(event.code === 'Enter'){
                            this.handleSubmit();
                        }
                    }}/>
                    <Button>Enviar</Button>
                </Message>
            </ChatContainer>
        )
    }
}

Chat.contextType = AuthContext;

export default Chat;