import env from '../env';

const apiUrl =  env.apiUrl;

class RoomService {
    async listAll(authToken){
        const result = await fetch(`${apiUrl}/rooms`, {
            method: 'GET',
            headers:{
                'Content-Type': 'application/json',
                'X-Auth': authToken
            }
        })
        const responseData = await result.json();

        if(result.status !== 200){ 
            const errorMessage = responseData.error;
            throw new Error(errorMessage)
        }
        return responseData;
    }

    async createRoom(authToken, data){
        const result = await fetch(`${apiUrl}/rooms`, {
            method: 'POST',
            headers:{
                'Content-Type': 'application/json',
                'X-Auth': authToken
            },
            body: JSON.stringify(data),
        })
        const responseData = await result.json();

        if(result.status !== 201){ 
            const errorMessage = responseData.error;
            throw new Error(errorMessage)
        }
        return responseData;
    }
}

export default RoomService;