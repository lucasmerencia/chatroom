import env from '../env';
const apiUrl =  env.apiUrl;

class UserService {
    async createAccount(data){
        if(data.password !== data.passwordConfirmation){
            throw new Error('password and confirmation must be equal')
        }

        const result = await fetch(`${apiUrl}/users`, {
            method: 'POST',
            headers:{
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data),
        })
        const responseData = await result.json();

        if(result.status !== 201){ 
            const errorMessage = responseData.error;
            throw new Error(errorMessage)
        }
        return responseData;
    }

    async login(data){
        const result = await fetch(`${apiUrl}/users/login`, {
            method: 'POST',
            headers:{
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data),
        })
        const responseData = await result.json();

        if(result.status !== 201){ 
            const errorMessage = responseData.error;
            throw new Error(errorMessage)
        }
        return responseData;
    }
}

export default UserService;