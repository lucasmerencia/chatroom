import styled from 'styled-components';

const Input = styled.input`
    display: block;
    height: 40px;
    width: 100%;
    line-height: 40px;
    border: 1px solid #124E78;
    border-radius: 5px;
    padding: 0 5px;
    outline: none;
    font-size: 15px;
`;

export default Input;