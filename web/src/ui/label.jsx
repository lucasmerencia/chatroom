import styled from 'styled-components';

const Label = styled.label`
    color: #124E78;
    font-size: 16px;
`;

export default Label;