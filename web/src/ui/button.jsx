import styled from 'styled-components';

const Button = styled.button`
    background-color: #124E78;
    color: #FFF;
    height: 40px;
    line-height: 40px;
    font-size: 16px;
    padding: 0 10px;
    min-width: 150px;
    border: 1px solid #013D67;
    border-radius: 5px;
    outline: none;
    &:active {
        background-color: #345F9A;
    }
`;

export default Button;