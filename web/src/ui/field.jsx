import React from 'react';
import Label from './label';
import Input from './input';
import styled from 'styled-components';

const FieldContained = styled.div`
    margin-bottom: 24px;
`;

const Field = ({ label, required, ...rest }) => {
    return (
        <FieldContained>
            <Label htmlFor={rest.name}>
                {label}
                { required && <span className="required">*</span>}
            </Label>
            <Input required={required} {...rest}/>
        </FieldContained>
    )
}

export default Field;