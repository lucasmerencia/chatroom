import styled from 'styled-components';

const Title = styled.h1`
    color: #124E78;
    font-weight: 100;
    margin-bottom: 30px;
`;

export default Title;