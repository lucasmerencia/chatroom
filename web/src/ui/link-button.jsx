import styled from 'styled-components';

import { Link } from 'react-router-dom'

const LinkButton = styled(Link)`
    color: #6d6d6d;
    height: 40px;
    line-height: 40px;
    font-size: 16px;
    padding: 0 10px;
    display: inline-block;
`;

export default LinkButton;