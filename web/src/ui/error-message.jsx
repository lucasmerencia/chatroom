import styled from 'styled-components';

const ErrorMessage = styled.div`
    display: flex;
    width: 100%;
    flex-flow: column;
    margin: auto auto;
    background-color: #FCDBD9;
    border: 1px #BB1711 solid;
    padding: 10px;
    color: #BB1711;
    margin-bottom: 20px;
`;

export default ErrorMessage;