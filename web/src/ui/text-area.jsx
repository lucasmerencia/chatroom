import styled from 'styled-components';

const TextArea = styled.textarea`
    display: block;
    height: 40px;
    width: 100%;
    line-height: 20px;
    border: 1px solid #124E78;
    border-radius: 5px;
    padding: 0 5px;
    outline: none;
    font-size: 15px;
    font-family: 'Open Sans', sans-serif;
`;

export default TextArea;