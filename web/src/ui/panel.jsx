import styled from 'styled-components';

const Panel = styled.div`
    display: flex;
    width: 100%;
    flex-flow: column;
    max-width: 400px;
    margin: auto auto;
    background-color: #FFF;
    padding: 24px;
    border-radius: 5px;
`;

export default Panel;