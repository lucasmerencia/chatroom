import React from 'react';
import { useAuth } from './auth-context';
import Login from './pages/login';
import CreateAccount from './pages/create-account';
import Home from './pages/home'
import CreateRoom from './pages/create-room'
import { Container } from './ui';

import {
    BrowserRouter as Router,
    Switch,
    Route
  } from "react-router-dom";


const Routes = () => {
    const { loggedIn } = useAuth();

    function getRoutes(){
        if(loggedIn){
            return (
            <Switch>
                <Route path="/create-room">
                    <CreateRoom />
                </Route>
                <Route path="/">
                    <Home />
                </Route>
            </Switch>
            )
        }

        return (
            <Switch>
            <Route path="/login">
                <Login />
            </Route>
            <Route path="/create-account">
                <CreateAccount />
            </Route>
            <Route path="/">
                <Login />
            </Route>
            </Switch>
        )
    } 

    return (
        <Router>
            <Container>
            { getRoutes() }    
            </Container>
        </Router>
    )
}

export default Routes;


