import React from 'react';

import { AuthProvider } from './auth-context';
import Routes from './routes';


function App() {
  return (
    <AuthProvider>
      <Routes/>
    </AuthProvider>
  );
}

export default App;
