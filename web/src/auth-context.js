import React, { createContext, useState, useContext, useEffect } from 'react';
import  AsyncStorage from '@react-native-community/async-storage';
import UserService from './services/user-service';

const userService = new UserService();

export const AuthContext = createContext({
    authToken: null,
    loggedIn: false,
    login: () => {},
    logout: () => {},
});

export const AuthProvider = ({children}) => {
    const [authToken, setAuthToken] = useState(null);
    const [username, setUsername] = useState(null);

    useEffect(() => {
        async function loadStorageDate(){
            const storedToken = await AsyncStorage.getItem('@ChatRoom:token');
            const storedUsername = await AsyncStorage.getItem('@ChatRoom:username');
            if(storedToken){
                setAuthToken(storedToken);
                setUsername(storedUsername);
            }
        }
        loadStorageDate();
    }, [authToken]);

    async function login(data){
        try {
            let result = await userService.login(data);
            let authToken = result.token;
            await AsyncStorage.setItem('@ChatRoom:token', authToken);
            await AsyncStorage.setItem('@ChatRoom:username', data.username);

            setAuthToken(authToken);
            setUsername(username);
        } catch (error) {
            throw error;
        }
    }

    async function logout(){
        await AsyncStorage.removeItem('@ChatRoom:token');
        await AsyncStorage.removeItem('@ChatRoom:username');
        setAuthToken(null);
    }

    const values = {
        loggedIn: !!authToken, 
        authToken, 
        login, 
        logout,
        username
    }

    return (
        <AuthContext.Provider value={values}>
            { children }
        </AuthContext.Provider>
    )
}


export function useAuth(){
    const context = useContext(AuthContext);
    return context;
}