const socket = require('socket.io');

const {CurrentRooms} = require('./features/room-state');
const {ReceiveMessage} = require('./features/room-state');

class SocketServer {
    constructor(){
        this.currentRooms = new CurrentRooms();
        this.receiveMessage = new ReceiveMessage();
    }

    createServer(httpServer){
        // TODO: Must set a origin for production.
        const io = socket(httpServer, {
            cors: {
                origin: '*',
                methods: ['GET', 'POST']
            }
        });
    
        io.on('connection', (client) => {
            client.on('access-room', async (data) => {
                this.currentRooms.accessRoom(data, client);
            });
    
            client.on('new-message', (data) => {
                this.receiveMessage.receive(data);
            });
    
            client.on('disconnect', () => {
                this.currentRooms.leaveAllRooms(client.id);
            });
        });
    
        return io;
    }
}

module.exports = SocketServer;