const db = require('./storage/postgres/db');

after(async () => {
    await db.destroy();
});

before(async () => {
    await db.migrate.latest();
});