const { assert } = require('chai');
const request = require('supertest');
const faker = require('faker');
const {CreateUser} = require('../features/user');

const app = require('../app');

describe('userRouter', () => {
    const createUser = new CreateUser();
    describe('/users', () => {
        describe('#POST', () => {
            it('should create a new user', async()=>{
                const response = await request(app).post('/users/')
                    .send({ username: 'myusername', password: 'mypassword'});
                assert.equal(response.status, 201);
                const user = response.body;
                assert.isNotNull(user.id);
                assert.isUndefined(user.encryptedPassword);
            });
    
            it('should fail with invalid user', async()=>{
                const response = await request(app).post('/users/')
                    .send({ username: 'myusername', password: ''});
                assert.equal(response.status, 400);
                assert.equal(response.body.error, 'password is required');
            });
        });
    });

    describe('/users/login', () => {
        let username;
        let password;

        beforeEach(async() => {
            username = faker.internet.userName();
            password = faker.internet.password();
            await createUser.create(username, password);
        });

        it('should create a token', async()=>{
            const response = await request(app).post('/users/login')
                .send({ username, password });
            assert.equal(response.status, 201);
            const authToken = response.body;
            assert.isNotNull(authToken.token);
        });

        it('should fail with invalid user', async()=>{
            const response = await request(app).post('/users/login')
                .send({ username: 'myusername', password: 'nopass'});
            assert.equal(response.status, 400);
            assert.equal(response.body.error, 'invlaid username or password');
        });
    });
});