const { Router } = require('express');

const router = new Router();

const logger = require('../tools/logger');

const { CreateRoom, ListAllRooms } = require('../features/room');

router.post('/', async (req, res) => {
    const currentUserId = res.locals.userId;
    const { name } = req.body;
    try{
        const createRoom = new CreateRoom();
        const room = await createRoom.create(currentUserId, name);
        res.status(201).send(room);
    } catch (error) {
        logger.warn(error.message);
        res.status(400).send({ error: error.message });
    }
});

router.get('/', async (_, res) => {
    const listAllRooms = new ListAllRooms();
    const rooms = await listAllRooms.list();
    res.status(200).send(rooms);
});

module.exports = router;