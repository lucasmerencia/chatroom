const { Router } = require('express');

const router = new Router();

const logger = require('../tools/logger');

const { CreateUser, CreateSession } = require('../features/user');

router.post('/', async (req, res) => {
    const { username, password } = req.body;
    try{
        const createUser = new CreateUser();
        const user = await createUser.create(username, password);
        delete user.encryptedPassword;
        res.status(201).send(user);
    } catch (error) {
        logger.warn(error.message);
        res.status(400).send({ error: error.message });
    }
});

router.post('/login', async (req, res) => {
    const { username, password } = req.body;
    try{
        const createSession = new CreateSession();
        const token = await createSession.create(username, password);
        res.status(201).send({token});
    } catch (error) {
        logger.warn(error.message);
        res.status(400).send({ error: error.message });
    }
});

module.exports = router;