const { Router } = require('express');

const userRouter = require('./user-router');
const roomRouter = require('./room-router');

const sessionMiddleware = require('./middleware/session-middleware');

const router = new Router();

router.get('/status', (_req, res) => {
    res.send({ http: 'ok' });
});

router.use('/users', userRouter);

router.use('/rooms', sessionMiddleware, roomRouter);

module.exports = router;