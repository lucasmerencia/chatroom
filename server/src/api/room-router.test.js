const { assert } = require('chai');
const request = require('supertest');
const { CreateUser, CreateSession} = require('../features/user');
const { CreateRoom } = require('../features/room');
const faker = require('faker');
const db = require('../storage/postgres/db');

const app = require('../app');

describe('/rooms', () => {
    const createUser = new CreateUser();
    const createSession = new CreateSession();
    const createRoom = new CreateRoom();

    let authToken;

    beforeEach(async() => {
        const username = faker.internet.userName();
        const password = faker.internet.password();
        await createUser.create(username, password);
        authToken = await createSession.create(username, password);
    });

    describe('#POST', () => {
        it('should create a new room', async()=>{
            const response = await request(app).post('/rooms/')
                .set('X-Auth', authToken)
                .send({ name: faker.random.word()});
            assert.equal(response.status, 201);
            const room = response.body;
            assert.isNotNull(room.id);
            assert.isNotEmpty(room.name);
        });

        it('should fail with invalid data', async()=>{
            const response = await request(app).post('/rooms/')
                .set('X-Auth', authToken)
                .send({ name: ''});

            assert.equal(response.status, 400);
            assert.equal(response.body.error, 'name is required');
        });
    });

    describe('#GET', () => {
        beforeEach(async () => {
            let user = await createUser.create(faker.internet.userName(), faker.internet.password());
            await db('rooms').truncate();
            for(let i = 1; i <= 10; i++){
                await createRoom.create(user.id, `Room ${i}`);
            }
        });

        it('should list all rooms', async () => {
            const response = await request(app).get('/rooms/').set('X-Auth', authToken);
            const rooms = response.body;

            assert.lengthOf(rooms, 10);
        });
    });
});