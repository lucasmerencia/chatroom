const jwt = require('jsonwebtoken');
const env = require('../../env');

module.exports = (req, res, next) => {
    let authToken = req.headers['x-auth'];

    if(!authToken){
        res.status(401).send({ error: 'Unauthorized'});
        return;
    }

    try {
        var decoded = jwt.verify(authToken, env().jwtSecret);
        res.locals.userId = decoded.userId;
        res.locals.userName = decoded.userName;
        next();
    } catch(err) {
        res.status(403).send({ error: 'Forbidden'});
    }
};