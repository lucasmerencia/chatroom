const sessionMiddleware = require('./session-middleware');
const jwt = require('jsonwebtoken');
const faker = require('faker');
const env = require('../../env');
const { assert } = require('chai');

describe('sessionMiddleware', () => {
    let token;
    let userId = faker.random.uuid();
    let userName = faker.internet.userName();

    beforeEach(() => {
        token = jwt.sign({
            userId: userId,
            userName: userName
        }, env().jwtSecret);
    });

    it('should add current user to response locals and delegate no next', () => {
        const request = {
            headers: {
                'x-auth': token
            }
        };

        const response = {
            locals: {}
        };

        let nextCalled = false;
        const next = () => {
            nextCalled = true;
        };
        
        sessionMiddleware(request, response, next);

        assert.equal(response.locals.userId, userId);
        assert.equal(response.locals.userName, userName);
        assert.isTrue(nextCalled);
    });

    it('should deny request with invalid token', () => {
        let invalidToken = jwt.sign({
            userId: userId,
            userName: userName
        }, 'invalid-secret');

        const request = {
            headers: {
                'x-auth': invalidToken
            }
        };

        let responseCode;
        let responseData;
        const response = {
            status(code){
                responseCode = code;
                return this;
            },
            send(data){
                responseData = data;
            }
        };

        let nextCalled = false;
        const next = () => {
            nextCalled = true;
        };
        
        sessionMiddleware(request, response, next);

        assert.equal(responseCode, 403);
        assert.equal(responseData.error, 'Forbidden');

        assert.isFalse(nextCalled);
    });

    it('should deny request without token', () => {
        const request = {
            headers: {}
        };

        let responseCode;
        let responseData;
        const response = {
            status(code){
                responseCode = code;
                return this;
            },
            send(data){
                responseData = data;
            }
        };

        let nextCalled = false;
        const next = () => {
            nextCalled = true;
        };
        
        sessionMiddleware(request, response, next);

        assert.equal(responseCode, 401);
        assert.equal(responseData.error, 'Unauthorized');
        
        assert.isFalse(nextCalled);
    });
});