const StockBot = require('./stock-bot');

class CommandHandler {
    constructor(){
        this.boots = [
            {
                pattern: /^\/stock=/i,
                instance: new StockBot('StockBot') 
            }
        ];
    }

    isCommand(text){
        return text.startsWith('/');
    }
    
    handleCommand(roomId, command){
        this.boots.forEach((bot) => {
            const match = bot.pattern.exec(command);
            if(match) bot.instance.receiveCommand(roomId, command);
        });
    }
}

module.exports = CommandHandler;