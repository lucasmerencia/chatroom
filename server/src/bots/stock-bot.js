const {GetQuote, StockCache} = require('../features/stocks');
const SendMessage = require('../features/room-state/send-message');
const uuid = require('uuid');
const logger = require('../tools/logger');

const commandPattern = /^\/stock=((\d|\w|\.)+)$/i;

class StockBot {
    constructor(name){
        this.name = name;
        this.getQuote = new GetQuote();
        this.sendMessage = new SendMessage();
        this.cache = new StockCache();
    }
    async fetchQuote(code){
        let quote = await this.cache.get(code);
        if(!quote){
            quote = await this.getQuote.get(code);
            this.cache.set(code, quote);
        }  
        return quote;
    }
    async receiveCommand(roomId, command){
        const match = commandPattern.exec(command);
        if(match){
            const code = match[1].toUpperCase();
            try{
                const quote = await this.fetchQuote(code);
                await this.sendMessage.send(roomId, {
                    id: uuid.v4(),
                    username: this.name,
                    message: `${code} quote is $${quote} per share.`,
                    roomId: roomId,
                    time: new Date() 
                }); 
                logger.info(`${this.name} replied to a command at ${roomId}`);
            } catch (error){
                await this.sendMessage.send(roomId, {
                    id: uuid.v4(),
                    username: this.name,
                    message: error.message,
                    roomId: roomId,
                    time: new Date() 
                });
                logger.info(`${this.name} replied to a command at ${roomId} with error`);
            }
        }
    }
}
module.exports = StockBot;