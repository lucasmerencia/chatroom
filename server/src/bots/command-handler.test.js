const { assert } = require('chai');
const CommandHandler = require('./command-handler');

describe('CommandHandler', () => {
    const commandHandler = new CommandHandler();
    describe('isCommand', () => {
        it('returns true when receive a command', () => {
            const result = commandHandler.isCommand('/command');
            assert.isTrue(result);
        });

        it('returns false when receive a text', () => {
            const result = commandHandler.isCommand('some text');
            assert.isFalse(result);
        });
    });

    describe('handleCommand', () => {
        it('delegates stock command to stock bot', () => {
            let receivedCommad;
            let receivedRoomId;
            commandHandler.boots[0].instance = {
                receiveCommand: (roomId, command) => {
                    receivedRoomId = roomId;
                    receivedCommad = command;
                }
            };

            commandHandler.handleCommand('some-room', '/stock=fb.us');

            assert.equal(receivedRoomId, 'some-room');
            assert.equal(receivedCommad, '/stock=fb.us');
        });

        it('igrones unknown commans', () => {
            let receivedCommad;
            commandHandler.boots[0].instance = {
                receiveCommand: (roomId, command) => {
                    receivedCommad = command;
                }
            };

            commandHandler.handleCommand('some-room', '/unknown');

            assert.isUndefined(receivedCommad);
        });
    });
});