
const StockBot = require('./stock-bot');
const {StockCache} = require('../features/stocks');
const { assert } = require('chai');

describe('StockBot', () => {
    const stockCache = new StockCache();
    let stockBot;
    let receivedCode;
    let receivedRoomId;
    let receivedMessage;

    beforeEach(() => {
        receivedCode = null;
        receivedRoomId = null;
        receivedMessage = null;
        stockBot = new StockBot();
        stockBot.getQuote.get = (code) => {
            receivedCode = code;
            return '10.0';
        };
        stockBot.sendMessage.send = (roomId, message) => {
            receivedRoomId = roomId;
            receivedMessage = message;
        };
    });
    
    it('ignores unknown commands', async() => {
        await stockBot.receiveCommand('roomId', '/unknown');
        assert.isNull(receivedCode);
    });

    it('sends the quote by message to the room', async() => {
        await stockBot.receiveCommand('roomId', '/stock=foo.us');
        assert.equal(receivedCode, 'FOO.US');
        assert.equal(receivedRoomId, 'roomId');
        assert.equal(receivedMessage.message, 'FOO.US quote is $10.0 per share.');
    });

    it('sends an error message to the room on fail', async() => {
        stockBot.getQuote.get =  () => { throw new Error('bot fail');};
        await stockBot.receiveCommand('roomId', '/stock=fb.us');
        assert.equal(receivedMessage.message, 'bot fail');
    });

    it('uses cache to answer the stock quote', async() => {
        await stockCache.set('SOME.US', '123.00');
        await stockBot.receiveCommand('roomId', '/stock=some.us');
        assert.equal(receivedMessage.message, 'SOME.US quote is $123.00 per share.');
    });
});