const env = require('../../env');
const jwt = require('jsonwebtoken');

const CommandHandler = require('../../bots/command-handler');
const SendMessage = require('./send-message');
const logger = require('../../tools/logger');

const redis = require('../../storage/redis/client');

class ReceiveMessage {
    constructor(){
        this.sendMessage = new SendMessage();
        this.commandHandler = new CommandHandler();
    }
    async receive(data) {
        const token = jwt.verify(data.authToken, env().jwtSecret);
        const messageText = data.message.trim();
        if(this.commandHandler.isCommand(messageText)){
            this.commandHandler.handleCommand(data.roomId, messageText);
        } else {
            let message = {
                id: data.id,
                username: token.userName,
                message: data.message,
                roomId: data.roomId,
                time: data.time 
            };
            redis.publish('message-topic', JSON.stringify(message));
            logger.info(`${token.userName} sent a message to ${ data.roomId }`);
        } 
    }
}

module.exports = ReceiveMessage;
