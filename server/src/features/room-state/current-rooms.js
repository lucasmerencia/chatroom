const env = require('../../env');
const jwt = require('jsonwebtoken');

const rooms = {};

const { ListMessages } = require('../message');

const logger = require('../../tools/logger');

class CurrentRoom {
    constructor(){
        this.listMessages = new ListMessages();
    }
    getClients(roomId){
        const room = rooms[roomId];
        if(room){
            return Object.keys(room).map((clinetId) => room[clinetId]);
        } 
        return [];
    }
    async accessRoom(data, client){
        const token = jwt.verify(data.authToken, env().jwtSecret);
        const roomId = data.roomId;
        this.leaveAllRooms(client.id);
        let currentRoom = rooms[roomId];
        if(!currentRoom){
            currentRoom = {};
        }
        currentRoom[client.id] = client;
        rooms[roomId] = currentRoom;

        let messages = await this.listMessages.list(data.roomId);
        client.emit('previous-messages', messages);
        logger.info(`${token.userName} accessed room ${data.roomId}`);
    }

    leaveAllRooms(clinetId){
        Object.keys(rooms).forEach((key) => {
            const room = rooms[key];
            if(Object.prototype.hasOwnProperty.call(room, clinetId)){
                delete room[clinetId];
            }
        } );
    }

}
 
module.exports = CurrentRoom;