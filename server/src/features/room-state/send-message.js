const CurrentRooms = require('./current-rooms');
const { SaveMessage } = require('../message');

class SendMessage {
    constructor(){
        this.saveMessage = new SaveMessage();
        this.currentRooms = new CurrentRooms();
    }

    async send(roomId, message){
        const sockets = this.currentRooms.getClients(roomId);
        await this.saveMessage.save(roomId, message);
        
        sockets.forEach(socket => {
            socket.emit('message-recieved', message);
        });
    }
}

module.exports = SendMessage;