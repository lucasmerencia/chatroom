const SendMessage = require('./send-message');
const CurrentRooms = require('./current-rooms');
const { assert } = require('chai');
const faker = require('faker');

const { CreateUser, CreateSession} = require('../user');

describe('SendMessage', () => {
    const createUser = new CreateUser();
    const createSession = new CreateSession();
    const sendMessage = new SendMessage();
    const currentRooms = new CurrentRooms();
    const roomId = 'someRoomId';

    let authToken;

    beforeEach(async () => {
        const username = faker.internet.userName();
        const password = faker.internet.password();

        await createUser.create(username, password);
        authToken = await createSession.create(username, password);
    });

    it('send a message to a room', async () => {
        let savedMessage;
        let savedAtRoomId;
        sendMessage.saveMessage = { save(roomId, message){
            savedAtRoomId = roomId;
            savedMessage = message;
        } };

        let emitedEvent;
        let messageSent;
        const client = {
            emit: (event, message) => {
                emitedEvent = event;
                messageSent = message;
            }            
        };
        await currentRooms.accessRoom({ authToken, roomId: roomId}, client);
        await sendMessage.send(roomId,  {message: 'a message'});

        assert.equal(savedAtRoomId, roomId);
        assert.equal(savedMessage.message, 'a message');
        assert.equal(emitedEvent, 'message-recieved');
        assert.equal(messageSent.message, 'a message');
    });
});