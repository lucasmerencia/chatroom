const { assert } = require('chai');
const CurrentRooms = require('./current-rooms');
const { CreateUser, CreateSession} = require('../user');
const faker = require('faker');

describe('CurrentRooms', () => {
    const createUser = new CreateUser();
    const createSession = new CreateSession();
    const currentRoom = new CurrentRooms();
    const roomId = 'room-id';

    let authToken;
    let secondAuth;

    beforeEach(async () => {
        let username = faker.internet.userName();
        let password = faker.internet.password();
        await createUser.create(username, password);
        authToken = await createSession.create(username, password);

        password = faker.internet.password();
        let secondUser = faker.internet.userName();
        await createUser.create(secondUser, password);
        secondAuth = await createSession.create(secondUser, password);
    });

    it('allow to access a room', async() => {
        const data = { 
            id: faker.random.uuid(),
            authToken,
            roomId,
            time: new Date()
        };
        currentRoom.listMessages.list = () => {
            return [ { message: 'message'} ];
        };
        let receivedEvent;
        let receivedMessages;
        const client = {
            id: faker.random.uuid(),
            emit: (event, messages) => {
                receivedEvent = event;
                receivedMessages = messages;
            }
        };
        await currentRoom.accessRoom(data, client);
        assert.equal(receivedEvent, 'previous-messages');
        assert.equal(receivedMessages[0].message, 'message');
    });

    it('allow many users to acess same room', async() => {
        const dataFistUser = { 
            id: faker.random.uuid(),
            authToken,
            roomId,
            time: new Date()
        };

        const dataSecondUser = { 
            id: faker.random.uuid(),
            authToken: secondAuth,
            roomId,
            time: new Date()
        }; 

        currentRoom.listMessages.list = () => {
            return [ { message: 'message'} ];
        };

        const clientFistUser = {
            id: faker.random.uuid(),
            emit: (event, messages) => {
                receivedEvent = event;
                receivedMessages = messages;
            }
        };

        let receivedEvent;
        let receivedMessages;
        const clientSecondUser = {
            id: faker.random.uuid(),
            emit: (event, messages) => {
                receivedEvent = event;
                receivedMessages = messages;
            }
        };

        await currentRoom.accessRoom(dataFistUser, clientFistUser);
        await currentRoom.accessRoom(dataSecondUser, clientSecondUser);

        assert.equal(receivedEvent, 'previous-messages');
        assert.equal(receivedMessages[0].message, 'message');
    });

    it('removes a user from all rooms', async () => {
        const data = { 
            id: faker.random.uuid(),
            authToken,
            roomId,
            time: new Date()
        };
        const clientId = faker.random.uuid();
        const client = {
            id: clientId,
            emit: () => {}
        };
        await currentRoom.accessRoom(data, client);
        await currentRoom.leaveAllRooms(clientId);

        const clients = currentRoom.getClients(roomId);
        
        assert.isEmpty(clients.filter(c => c.id === clientId));
    });

    it('get clients for users in the room', async () => {
        const newRoomId = 'new-room';
        const data = { 
            id: faker.random.uuid(),
            authToken,
            roomId: newRoomId,
            time: new Date()
        };
        const clientId = faker.random.uuid();
        const client = {
            id: clientId,
            emit: () => {}
        };
        await currentRoom.accessRoom(data, client);
        const clients = await currentRoom.getClients(newRoomId);
        assert.lengthOf(clients, 1);
    });

    it('returns emty when theres no user in the room', async () => {
        const clients = await currentRoom.getClients('empty-room');
        assert.lengthOf(clients, 0);
    });
});