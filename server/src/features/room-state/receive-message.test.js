const ReceiveMessage = require('./receive-message');

const { CreateUser, CreateSession} = require('../user');
const faker = require('faker');
const { assert } = require('chai');
const redis = require('../../storage/redis/client');
const sinon = require('sinon');

describe('ReceiveMessage', ()=>{
    const createUser = new CreateUser();
    const createSession = new CreateSession();
    const receiveMessage = new ReceiveMessage();
    let username;
        
    const roomId = 'someRoomId';

    let authToken;

    beforeEach(async () => {
        username = faker.internet.userName();
        const password = faker.internet.password();
        await createUser.create(username, password);
        authToken = await createSession.create(username, password);
    });

    it('receives a message', async ()=>{
        const data = { 
            id: faker.random.uuid(),
            username: username,
            authToken,
            message: 'some message',
            roomId,
            time: new Date()
        };
        let receivedTopic;
        let receivedMsg;

        
        const publishFn = sinon.stub(redis, 'publish');
        publishFn.callsFake((topic, value) => {
            receivedTopic = topic;
            receivedMsg = JSON.parse(value);
        });

        await receiveMessage.receive(data);
        assert.equal(receivedMsg.id, data.id);
        assert.equal(receivedTopic, 'message-topic');
    });

    it('delegate command to command handler', async ()=>{
        const data = { 
            id: faker.random.uuid(),
            username: username,
            authToken,
            message: '/stock=fb.us',
            roomId,
            time: new Date()
        };
        let receivedRoomId;
        let receivedText;

        receiveMessage.commandHandler.handleCommand = (roomId, messageText) => {
            receivedRoomId = roomId;
            receivedText = messageText; 
        };
        
        await receiveMessage.receive(data);
        console.log(receivedRoomId, receivedText);
        assert.equal(receivedText, '/stock=fb.us');
    });
});