const ListMessages = require('./list-messages');
const SaveMessage = require('./save-message');

module.exports = {
    ListMessages,
    SaveMessage
};