const redis = require('../../storage/redis/client');

const maxSize = 50;

class SaveMessage {
    save(roomId, message){
        return new Promise((resolve, reject) => {
            const roomKey = `room-${roomId}`;
            const multi = redis.multi();
            multi.rpush(roomKey, JSON.stringify(message));
            multi.ltrim(roomKey, maxSize * -1, -1);
            multi.exec((error, reply) => {
                if(error) reject(error);
                else resolve(reply);
            });
        });
    }
}

module.exports = SaveMessage;