const { assert } = require('chai');
const SaveMessage = require('./save-message');
const redis = require('../../storage/redis/client');
const sinon = require('sinon');

describe('SaveMessage', () => {
    const saveMessage = new SaveMessage();
    beforeEach(() => {
        sinon.restore();
        redis.flushall();
    });

    it('should save a message', (done) => {
        const message = {message: 'some message'};
        saveMessage.save('roomid', message);
        redis.lpop('room-roomid', (_, msg) => {
            assert.equal(msg, JSON.stringify(message));
            done();
        });
    });

    it('should save a message', (done) => {
        const message = {message: 'some message'};
        saveMessage.save('roomid', message);
        redis.lpop('room-roomid', (_, msg) => {
            assert.equal(msg, JSON.stringify(message));
            done();
        });
    });

    it('should save only 50 messages', (done) => {
        const roomId = 'roomid';
        const savesPormises = [];
        for(let i = 1; i <= 60; i++){
            const message = {message: `message ${i}`};
            savesPormises.push(saveMessage.save(roomId, message));
        }
       
        Promise.all(savesPormises).then(() => {
            redis.lrange(`room-${roomId}`, 0, -1, (_, messages) => {
                let savedMessages = messages.map(m => JSON.parse(m));
                assert.lengthOf(savedMessages, 50);
                assert.equal(savedMessages[0].message, 'message 11' );
                assert.equal(savedMessages[24].message, 'message 35' );
                assert.equal(savedMessages[49].message, 'message 60' );
                done();
            });
        });
    });

    it('fails if redis fails', async () => {
        const multi = redis.multi();
        const multiFn = sinon.stub(redis, 'multi');
        const execFn = sinon.stub(multi, 'exec');
        multiFn.returns(multi);

        execFn.callsFake((callback)=>{
            callback(new Error('an error'));
        });

        try{
            await saveMessage.save('roomid', {message: 'msg'});
            assert.fail('must fail on redis error');
        } catch (err) {
            assert.equal(err.message, 'an error');
        }
    });
});