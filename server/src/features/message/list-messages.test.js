const { assert } = require('chai');
const SaveMessage = require('./save-message');
const ListMessages = require('./list-messages');
const redis = require('../../storage/redis/client');
const sinon = require('sinon');

describe('ListMessages', () => {
    const saveMessage = new SaveMessage();
    const listMessages = new ListMessages();
    const roomId = 'room-id';

    beforeEach(async () => {
        sinon.restore();
        redis.flushall();
    });

    it('should list messages', async () => {
        for(let i = 1; i <= 10; i++){
            saveMessage.save(roomId, {message: `message ${i}`});
        }
        let messages = await listMessages.list(roomId);
        assert.lengthOf(messages, 10);
        assert.equal(messages[9].message, 'message 10');
    });

    it('fails if redis fails', async () => {
        const lrangeFn = sinon.stub(redis, 'lrange');
        lrangeFn.callsFake((key, start, stop, callback)=>{
            callback(new Error('some error'));
        });

        try{
            await listMessages.list(roomId);
            assert.fail('must fail on redis error');
        } catch (err) {
            assert.equal(err.message, 'some error');
        }
    });
});



