const redis = require('../../storage/redis/client');

class ListMessages {
    list(roomId){
        return new Promise((resolve, reject) => {
            const roomKey = `room-${roomId}`;
            redis.lrange(roomKey, 0, -1, (error, messages) => {
                if(error) reject(error);
                else resolve(messages.map(m => JSON.parse(m)));
            });
        });
    }
}

module.exports = ListMessages;