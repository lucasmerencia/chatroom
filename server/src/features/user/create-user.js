const { UserRepository } = require('../../storage');
const env = require('../../env');
const cypher = require('../../tools/cypher');

class CreateUser {
    constructor(){
        this.repository = new UserRepository();
    }

    create(username, password){
        if(!username) throw new Error('username is required');
        if(username.length < 4) throw new Error('username should have at least 4 characters');

        if(!password) throw new Error('password is required');
        if(password.length < 8) throw new Error('password should have at least 8 characters');

        const encryptedPassword = cypher.hash(password, env().secret);
        
        return this.repository.insert({ username, encryptedPassword });
    }
}

module.exports = CreateUser;