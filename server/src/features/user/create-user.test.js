const CreateUser = require('./create-user');
const { assert } = require('chai');

describe('CreateUser', () => {
    const createUser = new CreateUser();
    
    it('should create a new user', async() => {
        const user = await createUser.create('myuser', 'strongpass');
        assert.isNotNull(user);
        assert.isNotNull(user.id);
    });

    it('should fail without username', async() => {
        try {
            await createUser.create(null, 'strongpass');
            assert.fail('expected to fail without username');
        } catch (error){
            assert.equal(error.message, 'username is required');
        }
    });

    it('should fail with empty username', async() => {
        try {
            await createUser.create('', 'strongpass');
            assert.fail('expected to fail with empty username');
        } catch (error){
            assert.equal(error.message, 'username is required');
        }
    });

    it('should fail with too short username', async() => {
        try {
            await createUser.create('hi', 'strongpass');
            assert.fail('expected to fail with too short username');
        } catch (error){
            assert.equal(error.message, 'username should have at least 4 characters');
        }
    });

    it('should fail without password', async() => {
        try {
            await createUser.create('user', null);
            assert.fail('expected to fail without password');
        } catch (error){
            assert.equal(error.message, 'password is required');
        }
    });

    it('should fail with empty password', async() => {
        try {
            await createUser.create('user', '');
            assert.fail('expected to fail with empty password');
        } catch (error){
            assert.equal(error.message, 'password is required');
        }
    });

    it('should fail with too short password', async() => {
        try {
            await createUser.create('user', 'pass');
            assert.fail('expected to fail with too short password');
        } catch (error){
            assert.equal(error.message, 'password should have at least 8 characters');
        }
    });
});