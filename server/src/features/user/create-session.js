const { UserRepository } = require('../../storage');
const cypher = require('../../tools/cypher');
const jwt = require('jsonwebtoken');
const env = require('../../env');

class CreateSession {
    constructor(){
        this.repo = new UserRepository();
    }

    async create(username, password){
        const user = await this.repo.findByUsername(username);
        if(!user){
            throw new Error('invlaid username or password');
        }

        const encryptedPassword = cypher.hash(password, env().secret);

        if(user.encryptedPassword === encryptedPassword){
            return jwt.sign({ 
                userId: user.id,
                userName: user.username
            }, env().jwtSecret, {
                expiresIn: '1 day'
            });
        } else {
            throw new Error('invlaid username or password');
        }
    }
}


module.exports = CreateSession;