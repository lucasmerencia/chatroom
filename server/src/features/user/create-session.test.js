const CreateSession = require('./create-session');
const CreateUser = require('./create-user');
const { assert } = require('chai');
const faker = require('faker');
const jwt = require('jsonwebtoken');
const env = require('../../env');

describe('CreateSession', () => {
    const createSession = new CreateSession();
    const createUser = new CreateUser();
    let userName;
    let password;
    let user;

    beforeEach(async () => {
        userName = faker.internet.userName();
        password = faker.internet.password();
        user = await createUser.create(userName, password);
    });

    it('should generate a jwt token', async () => {
        const token = await createSession.create(userName, password);
        const decoded = jwt.decode(token, env().jwtSecret);
        assert.isNotNull(token);
        assert.equal(decoded.userId, user.id);
        assert.equal(decoded.userName, user.username);
  
    });

    it('should fail if user not found', async () => {
        try{
            await createSession.create(faker.internet.userName(), faker.internet.password());
            assert.fail('must fail with invalid user');
        } catch (error){
            assert.equal(error.message, 'invlaid username or password');
        }
    });

    it('should fail if password was wrong', async () => {
        try{
            await createSession.create(userName, faker.internet.password());
            assert.fail('must fail with wrong password');
        } catch (error){
            assert.equal(error.message, 'invlaid username or password');
        }
    });
});