const CreateRoom = require('./create-room');
const ListAllRooms = require('./list-all-rooms');

module.exports = {
    CreateRoom,
    ListAllRooms
};