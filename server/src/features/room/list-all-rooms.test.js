const ListAllRooms = require('./list-all-rooms');
const CreateRoom = require('./create-room');
const { CreateUser } = require('../user');
const { assert } = require('chai');
const faker = require('faker');
const db = require('../../storage/postgres/db');

describe('ListAllRooms', () => {
    const createRoom = new CreateRoom();
    const createUser = new CreateUser();
    const listAllRooms = new ListAllRooms();
    let user;

    beforeEach(async () => {
        user = await createUser.create(faker.internet.userName(), faker.internet.password());
        await db('rooms').truncate();
        for(let i = 1; i <= 10; i++){
            await createRoom.create(user.id, `Room ${i}`);
        }
    });

    it('list all rooms', async () => {
        const rooms = await listAllRooms.list();
        assert.lengthOf(rooms, 10);
        assert.equal(rooms[9].name, 'Room 10');
    });
    
});