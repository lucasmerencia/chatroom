const {RoomRepository} = require('../../storage');

class ListAllRooms {
    constructor(){
        this.repository = new RoomRepository(); 
    }

    list(){
        return this.repository.list();
    }
}

module.exports = ListAllRooms;