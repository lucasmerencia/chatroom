const CreateRoom = require('./create-room');
const { assert } = require('chai');
const faker = require('faker');

const { CreateUser } = require('../user');

describe('CreateRoom', () => {
    const createRoom = new CreateRoom();
    const createUser = new CreateUser();
    let user;

    beforeEach(async () => {
        user = await createUser.create(faker.internet.userName(), faker.internet.password());
    });
    
    it('should create a new room', async() => {
        const room = await createRoom.create(user.id, 'random');
        assert.isNotNull(room);
        assert.isNotNull(room.id);
    });

    it('should fail without name', async() => {
        try {
            await createRoom.create(user.id);
            assert.fail('expected to fail without name');
        } catch (error){
            assert.equal(error.message, 'name is required');
        }
    });

    it('should fail without createdBy', async() => {
        try {
            await createRoom.create(null, 'my room');
            assert.fail('expected to fail without createdBy');
        } catch (error){
            assert.equal(error.message, 'createdBy is required');
        }
    });

    it('should fail when user given at createdBy was not found', async() => {
        try {
            await createRoom.create(faker.random.uuid(), 'my room');
            assert.fail('expected to fail witho invalid user');
        } catch (error){
            assert.equal(error.message, 'user not found');
        }
    });

    it('should fail if room name alredy exists', async () => {
        await createRoom.create(user.id, 'myroom');
        try {
            await createRoom.create(user.id, 'myroom');
            assert.fail('must fail if room alredy exists');
        } catch (error){
            assert.equal(error.message, 'room myroom alredy exists');
        }
    });

});