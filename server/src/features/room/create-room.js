const { RoomRepository, UserRepository } = require('../../storage');

class CreateRoom {
    constructor(){
        this.roomRepository = new RoomRepository();
        this.userRepository = new UserRepository();
    }

    async create(createdBy, name){
        if(!name) throw new Error('name is required');
        if(!createdBy) throw new Error('createdBy is required');

        const creator = await this.userRepository.find(createdBy);
        if(!creator) throw new Error('user not found');

        const currentRoom = await this.roomRepository.findByName(name);
        if(currentRoom) throw new Error(`room ${name} alredy exists`);

        return this.roomRepository.insert({ createdBy, name });
    }
}

module.exports = CreateRoom;