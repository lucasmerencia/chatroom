const StockCache = require('./stock-cache');
const { assert } = require('chai');
const redis = require('../../storage/redis/client');
const sinon = require('sinon');

function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

describe('StockCache', () => {
    const cache = new StockCache();

    it('keeps a value in cache', async() => {
        await cache.set('some-code', '100');
        let value = await cache.get('some-code');
        assert.equal(value, '100');
    });

    it('expires a value', async()=>{
        await cache.set('foo.us', '10', 1);
        await timeout(1200);
        let value = await cache.get('foo.us');
        assert.isNull(value);
    });

    it('returns null if redis fais on get quote', async() => {
        const getFn = sinon.stub(redis, 'get');
        getFn.callsFake((key, callback) => {
            callback(new Error('some error'));
        });

        const value = await cache.get('foo.us');
        assert.isNull(value);
        getFn.restore();
    });

    it('not fails if redis fails on set quote', async() => {
        const setexFn = sinon.stub(redis, 'setex');
        setexFn.callsFake((key, value, ttl, callback) => {
            callback(new Error('some error'));
        });
        try {
            await cache.set('foo.us', '100');
        } catch(error) {
            assert.fail(error.message);
        }
        setexFn.restore();
    });
});