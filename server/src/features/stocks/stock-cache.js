const redis = require('../../storage/redis/client');
const logger = require('../../tools/logger');

class StockCache {
    set(code, value, ttl = 60){
        return new Promise((resolve) => {
            redis.setex(`sock-quote-${code}`, ttl, value, (error, reply) => {
                if(error) {
                    logger.error(error.message);
                    resolve();
                }
                else resolve(reply);
            });
        });
    }

    get(code){
        return new Promise((resolve) => {
            redis.get(`sock-quote-${code}`, (error, reply) => {
                if(error){ 
                    logger.error(error.message);
                    resolve(null);
                }
                else resolve(reply);
            });
        });
    }
}

module.exports = StockCache;