const GetQuote = require('./get-quote');
const nock = require('nock');

const fs = require('fs');
const path = require('path');
const { assert } = require('chai');

describe('GetQuote', () => {
    const getQuote = new GetQuote();
    describe('getting a quote', ()=> {
        const code = 'aapl.us';
        before(() => {
            const assetPath = path.resolve(__dirname, '../../../test-assets/aapl.us.csv');
            const response = fs.readFileSync(assetPath);
            nock('https://stooq.com')
                .get(`/q/l/?s=${code}&f=sd2t2ohlcv&h&e=csv`)
                .reply(200, response);
        });

        it('gets a quote', async ()=> {
            const quote = await getQuote.get(code);
            assert.equal(quote, '119.99');
        });
    });

    describe('quote not found', () => {
        const code = 'appl.us';
        before(() => {
            const assetPath = path.resolve(__dirname, '../../../test-assets/appl.us-empty.csv');
            const response = fs.readFileSync(assetPath);
            nock('https://stooq.com')
                .get(`/q/l/?s=${code}&f=sd2t2ohlcv&h&e=csv`)
                .reply(200, response);
        });

        it('fails when quote was N/D', async ()=> {
            try{
                await getQuote.get(code);
                assert.fail('must fail when quote was N/D');
            } catch(error){
                assert.equal(error.message, `there is no quote for ${code}`);
            }
        });
    });

    describe('failing request', () => {
        const code = 'appl.us';
        before(() => {
            nock('https://stooq.com')
                .get(`/q/l/?s=${code}&f=sd2t2ohlcv&h&e=csv`)
                .reply(400, 'HTTP FAIL');
        });

        it('fails if http request fails', async ()=> {
            try{
                await getQuote.get(code);
                assert.fail('must fail when http request fails');
            } catch(error){
                assert.equal(error.message, 'Response code 400 (Bad Request)');
            }
        });
    });

    describe('invalid csv', () => {
        const code = 'appl.us';
        before(() => {
            const assetPath = path.resolve(__dirname, '../../../test-assets/invalid.csv');
            const response = fs.readFileSync(assetPath);
            nock('https://stooq.com')
                .get(`/q/l/?s=${code}&f=sd2t2ohlcv&h&e=csv`)
                .reply(200, response);
        });

        it('fails when csv was invalid', async ()=> {
            try{
                await getQuote.get(code);
                assert.fail('must fail when csv was invalid');
            } catch(error){
                assert.equal(error.message, 'cannot get a quote, invalid csv');
            }
        });
    });
});