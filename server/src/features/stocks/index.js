const GetQuote = require('./get-quote');
const StockCache = require('./stock-cache');

module.exports = {
    GetQuote,
    StockCache
};