const got = require('got');
const parse = require('csv-parse');

async function getCsv(code){
    const url = `https://stooq.com/q/l/?s=${code}&f=sd2t2ohlcv&h&e=csv`;
    const response = await got(url);
    return response.body;
}

function parseCsv(text){
    return new Promise((resolve, reject) => {
        parse(text, (err, records) => {
            if(err) reject(new Error('cannot get a quote, invalid csv'));
            else resolve(records);
        });
    });
}

class GetQuote {
    async get(code){
        const csvText = await getCsv(code.toLowerCase());
        const records = await parseCsv(csvText);
        const quote = records[1][6];
        if(quote === 'N/D') throw new Error(`there is no quote for ${code}`); 
        return quote;
    }
}

module.exports = GetQuote;