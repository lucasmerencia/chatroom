const MessageListener = require('./message-listener');
const consumer = require('./storage/redis/consumer');
const { assert } = require('chai');
const sinon = require('sinon');

describe('MessageListener', () => {
    const messageListener = new MessageListener();

    after(() => {
        messageListener.close();
    });

    it('listen message topic', () => {
        const onFn = sinon.stub(consumer, 'on');
        const subscribeFn = sinon.stub(consumer, 'subscribe');

        let receivedEvent;
        let receivedRoomId;
        let receivedMessage;

        onFn.callsFake((event, listener) => {
            receivedEvent = event;
            listener('message-topic', JSON.stringify({ roomId:'roomId', message: 'some message'}));
        } );

        messageListener.sendMessage.send = (roomId, message) => {
            receivedRoomId = roomId;
            receivedMessage = message;
        };

        messageListener.listen();
        const listenTopic = subscribeFn.getCall(0).args[0];

        assert.equal(receivedMessage.message, 'some message');
        assert.equal(receivedRoomId, 'roomId');
        assert.equal(receivedEvent, 'message');
        assert.equal(listenTopic, 'message-topic');

        sinon.restore();
    });

    it('ignore other topics', () => {
        const onFn = sinon.stub(consumer, 'on');
        let receivedEvent;

        onFn.callsFake((event, listener) => {
            receivedEvent = event;
            listener('other-topic', 'message');
        } );

        let called = false;
        messageListener.sendMessage.send = () => {
            called = true;
        };

        messageListener.listen();

        assert.equal(receivedEvent, 'message');
        assert.isFalse(called);

        sinon.restore();
    });
});