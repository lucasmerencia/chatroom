
const { createServer } = require('http');
const socketClient = require('socket.io-client');
const Socket = require('./socket');

const { CreateUser, CreateSession } = require('./features/user');
 
const { assert } = require('chai');
const faker = require('faker');

describe('socket', () => {
    const socket = new Socket();
    const createUser = new CreateUser();
    const createSession = new CreateSession();
    let clientSocket;
    let serverSocket;
    
    let authToken;

    before((done) => {
        const httpServer = createServer();
        serverSocket = socket.createServer(httpServer);
        httpServer.listen(() => {
            const port = httpServer.address().port;
            clientSocket = new socketClient.io(`http://localhost:${port}`);
            clientSocket.on('connect', done);
        });
    });
    
    after(() => {
        serverSocket.close();
        clientSocket.close();
    });

    beforeEach(async() => {
        const username = faker.internet.userName();
        const password = faker.internet.password();
        await createUser.create(username, password);
        authToken = await createSession.create(username, password);
    });

    it('accept new clients in a room', (done) => {
        const data = {
            roomId: faker.random.uuid(),
            authToken
        };

        socket.currentRooms.accessRoom = (data) => {
            assert.equal(data.authToken, authToken);
            done();
        };

        clientSocket.emit('access-room', data);
    });

    it('receives new messages', (done) => {
        const data = {
            id: faker.random.uuid(),
            roomId: faker.random.uuid(),
            authToken,
            message: 'some message',
            time: new Date()
        };

        socket.receiveMessage.receive = (data) => {
            assert.equal(data.authToken, authToken);
            assert.equal(data.message, 'some message');
            done();
        };

        clientSocket.emit('new-message', data);
    });
});