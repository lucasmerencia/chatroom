const db = require('./db');
const uuid = require('uuid');

class PgCrudRepository {
    constructor(table){
        this.table = table;
    }

    async insert(data){
        if(!data.id || !uuid.validate(data.id)){
            data.id = uuid.v4();
        }
        data.createdAt = new Date();
        data.updatedAt = new Date();
        await db(this.table).insert(data);
        return data;
    }

    async find(id){
        let result = await db(this.table).where({id}).whereNull('deleted_at');
        if (result && result.length > 0){
            return result[0];
        }
        return null;
    }

    list(){
        return db(this.table).whereNull('deleted_at');
    }
}


module.exports = PgCrudRepository;