const CrudRepository = require('./crud-repository');
const { assert } = require('chai');
const uuid = require('uuid');
const db = require('./db');

describe('CrudRepositroy', () => {
    const repository = new CrudRepository('entities');
    
    before(async () => {
        await db.schema.createTable('entities', (table) => {
            table.string('id').primary();
            table.timestamp('created_at').notNullable();
            table.timestamp('updated_at').notNullable();
            table.timestamp('deleted_at');
            table.string('name').notNullable();
        });
    });
    
    after(async () => {
        await db.schema.dropTable('entities');
    });
    
    describe('insert', () => {
        let entity;

        beforeEach(async() => {
            entity = await repository.insert({ name: 'Some Entity' });
        });
        
        it('should generate an id', () => {
            assert.isDefined(entity.id);
            assert.isTrue(uuid.validate(entity.id));
        });

        it('should generate created at', () => {
            assert.isDefined(entity.createdAt);
            assert.instanceOf(entity.createdAt, Date);
        });

        it('should generate updated at', () => {
            assert.isDefined(entity.updatedAt);
            assert.instanceOf(entity.updatedAt, Date);
        });

        it('should insert an entity without generate id', async() => {
            const id = uuid.v4();
            const newEntity = await repository.insert({ id, name: 'Some Entity' });
            assert.equal(newEntity.id, id);
        });

        it('should generate an id if the provided one was invalid', async() => {
            const newEntity = await repository.insert({ id: 'invalid-uuid', name: 'Some Entity' });
            assert.notEqual(newEntity.id, 'invalid-uuid');
            assert.isTrue(uuid.validate(entity.id));
        });
    });

    describe('find', () => {
        let entityId;
        beforeEach(async() => {
            const entity = await repository.insert({ name: 'Some Entity' });
            entityId = entity.id;
        });
        
        it('should find an entity by id', async () => {
            const entity = await repository.find(entityId);
            assert.isNotNull(entity);
            assert.equal(entity.id, entityId);
        });

        it('should return null when not found an entity', async () => {
            const entity = await repository.find('invalid id');
            assert.isNull(entity);
        });
    });

    describe('list', () => {
        beforeEach(async() => {
            await db('entities').truncate();
            for(let i = 1; i <= 10; i++){
                await repository.insert({ name: `Entity ${i}` });
            }
        });
        
        it('should list all entities', async () => {
            const entities = await repository.list();
            assert.lengthOf(entities, 10);
            assert.equal(entities[9].name, 'Entity 10');
        });
    });
});