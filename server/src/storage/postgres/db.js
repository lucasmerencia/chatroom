const knex = require('knex');
const knexStringcase = require('knex-stringcase');

const knexfile = require('../../../knexfile');
const env = require('../../env');

function prop(obj, key) {
    return obj[key];
}

const options = prop(knexfile, env().node);

const connection = knex(knexStringcase(options));

module.exports = connection;