const PgCrudRepository = require('./postgres/crud-repository');
const db = require('./postgres/db');

class UserRepository extends PgCrudRepository{
    constructor(){
        super('users');
    }

    async findByUsername(username){
        let result = await db(this.table).where({username});
        if (result && result.length > 0){
            return result[0];
        }
        return null;
    }
}

module.exports = UserRepository;