const PgCrudRepository = require('./postgres/crud-repository');
const db = require('./postgres/db');

class RoomRepository extends PgCrudRepository{
    constructor(){
        super('rooms');
    }

    async findByName(name){
        const result = await db(this.table).where({ name }).whereNull('deletedAt');
        if (result && result.length > 0){
            return result[0];
        }
        return null;
    }
}

module.exports = RoomRepository;