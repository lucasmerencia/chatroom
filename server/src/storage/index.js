const UserRepository = require('./user-repository');
const RoomRepository = require('./room-repository');

module.exports = {
    UserRepository,
    RoomRepository
};