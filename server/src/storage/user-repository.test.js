const UserRepository = require('./user-repository');
const CrudRepository = require('./postgres/crud-repository');
const {assert} = require('chai');

describe('UserRepository', () => {
    const repository = new UserRepository();
    
    it('is a type of CrudRepository', async () => {
        assert.instanceOf(repository, CrudRepository);
    });

    describe('findByUsername', () => {
        it('should find an user by username', async() => {
            await repository.insert({ username: 'someuser', encryptedPassword: 'strong password'});
            const user = await repository.findByUsername('someuser');
            assert.equal(user.username, 'someuser');
        });

        it('should return null if not found an user', async() => {
            const user = await repository.findByUsername('invalidusername');
            assert.isNull(user);
        });
    });
});