const { assert } = require('chai');

const consumer = require('./consumer');

describe('Redis Consumer', () => {
    after(() => {
        consumer.quit();
    });
    it('should connect to redis', (done) => {
        consumer.ping((_, reply) => {
            console.log(reply);
            assert.equal(reply, 'PONG');
            done();
        }); 
    });
});