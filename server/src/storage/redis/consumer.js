const Redis = require('ioredis');
const env = require('../../env');
const client = new Redis(env().redisUrl);
module.exports = client;