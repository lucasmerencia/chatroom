const { assert } = require('chai');

const client = require('./client');

describe('Redis Client', () => {
    after(() => {
        client.quit();
    });
    it('should connect to redis', (done) => {
        client.ping((_, reply) => {
            console.log(reply);
            assert.equal(reply, 'PONG');
            done();
        }); 
    });
});