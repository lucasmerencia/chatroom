const RoomRepository = require('./room-repository');
const UserRepository = require('./user-repository');
const CrudRepository = require('./postgres/crud-repository');
const {assert} = require('chai');
const faker = require('faker');

describe('RoomRepository', () => {
    const repository = new RoomRepository();
    const userRepository = new UserRepository();
    
    it('is a type of CrudRepository', async () => {
        assert.instanceOf(repository, CrudRepository);
    });

    describe('findByName', () => {
        let userId;

        beforeEach(async() => {
            const user = await userRepository.insert({username: faker.internet.userName(), encryptedPassword: 'somepass'});
            userId = user.id;
        });
        it('should find a room by name', async() => {
            await repository.insert({ name: 'some-room', createdBy: userId  });
            const room = await repository.findByName('some-room');
            assert.equal(room.name, 'some-room');
        });

        it('should return null if not found a room', async() => {
            const room = await repository.findByName('invalidroom');
            assert.isNull(room);
        });
    });
});