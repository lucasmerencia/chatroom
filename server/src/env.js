function env(){
    const port = process.env.PORT || '3030';
    return {
        port: parseInt(port),
        node: process.env.NODE_ENV || 'development',
        secret: process.env.SECRET || 'app-secret',
        jwtSecret: process.env.JWT_SECRET || 'jwt-secret',
        redisUrl: process.env.REDIS_URL || 'redis://localhost:6379',
    };
}

module.exports = env;
