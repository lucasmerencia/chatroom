const env = require('./env');
const { assert } = require('chai');

describe('env', () => {
    describe('port', async () => {
        it('use defalut value', () => {
            const oldPort = process.env.PORT;
            delete process.env.PORT;
            assert.equal(env().port, 3030);
            process.env.PORT = oldPort;
        });
        it('use custom value', () => {
            const oldPort = process.env.PORT;
            process.env.PORT = '5000';
            assert.equal(env().port, 5000);
            process.env.PORT = oldPort;
        });
    });

    describe('node', async () => {
        it('use defalut value', () => {
            const oldNode = process.env.NODE_ENV;
            delete process.env.NODE_ENV;
            assert.equal(env().node, 'development');
            process.env.NODE_ENV = oldNode;
        });

        it('use custom value', () => {
            const oldNode = process.env.NODE_ENV;
            process.env.NODE_ENV = 'envv';
            assert.equal(env().node, 'envv');
            process.env.NODE_ENV = oldNode;
        });
    });

    describe('secret', async () => {
        it('use defalut value', () => {
            const oldSecret = process.env.SECRET;
            delete process.env.SECRET;
            assert.equal(env().secret, 'app-secret');
            process.env.SECRET = oldSecret;
        });

        it('use custom value', () => {
            const oldSecret = process.env.SECRET;
            process.env.SECRET = 'other-secret';
            assert.equal(env().secret, 'other-secret');
            process.env.SECRET = oldSecret;
        });
    });

    describe('jwtSecret', async () => {
        it('use defalut value', () => {
            const oldSecret = process.env.JWT_SECRET;
            delete process.env.JWT_SECRET;
            assert.equal(env().jwtSecret, 'jwt-secret');
            process.env.JWT_SECRET = oldSecret;
        });

        it('use custom value', () => {
            const oldSecret = process.env.JWT_SECRET;
            process.env.JWT_SECRET = 'other-secret';
            assert.equal(env().jwtSecret, 'other-secret');
            process.env.JWT_SECRET = oldSecret;
        });
    });
});