const cypher = require('./cypher');
const { assert } = require('chai');

describe('cypher', () => {
    it('encrypt without salt', () => {
        const encoded = cypher.hash('some text');
        assert.equal(encoded, '7f7457138e0931f74c0ee086025f364ffe06eb27f55059209929a31e821b0a8fec299d22ab795dd2fe720842bba589c2a73e95ac7ce77c3775b1bf671452672d');
    });

    it('encrypt with salt', () => {
        const encoded = cypher.hash('some text', 'salt');
        assert.equal(encoded, '7c90741fb6fc034356d47e8fbd1bdedf90e26b6aa646bdeed43db7b1326dbfbb30a12057d61252add218d68d2e78616a56cb53b9772c2da19cae6cc9ac8d310f');
    });
});