const { assert } = require('chai');
const sinon = require('sinon');
const logger = require('./logger');

describe('logger', () => {
    it('should send logs to console', () => {
        const spy = sinon.spy(console._stdout, 'write');
        logger.info('some log');
        assert.isTrue(spy.calledOnce);
        assert.include(spy.getCalls()[0].args[0], 'some log');
    });
});