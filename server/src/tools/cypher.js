const crypto = require('crypto');

module.exports = {
    hash(text, secret = '') {
        let hmac = crypto.createHmac('sha512', secret);
        return hmac.update(text).digest('hex');
    }
};