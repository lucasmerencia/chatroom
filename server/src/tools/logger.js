const winston = require('winston');

const logger = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    defaultMeta: { service: 'web' },
    transports: [
        new winston.transports.Console({
            format: winston.format.cli(),
        })
    ],
});

module.exports = logger;