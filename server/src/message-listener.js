const redis = require('./storage/redis/consumer');
const SendMessage = require('./features/room-state/send-message');

class MessageListener{
    constructor(){
        this.sendMessage = new SendMessage();
    }
    listen(){
        redis.subscribe('message-topic');

        redis.on('message', (topic, data) => {
            if(topic === 'message-topic'){
                let message = JSON.parse(data);
                this.sendMessage.send(message.roomId, message);
            }
        });
    }
    close(){
        redis.quit();
    }
}

module.exports = MessageListener;