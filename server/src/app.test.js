const { assert } = require('chai');
const request = require('supertest');

const app = require('./app');

describe('app', () => {
    it('should mount api', async () => {
        const response = await request(app).get('/status');
        assert.equal(response.body.http, 'ok');
    });
});