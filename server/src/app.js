const http = require('http');
const express = require('express');
const cors = require('cors');

const api = require('./api');

const Socket = require('./socket');
const socket = new Socket();

const MessageListener = require('./message-listener');
const messageListener = new MessageListener();

const app = express();
app.options('*', cors());
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));

app.use('/', api);

const server = http.Server(app);
socket.createServer(server);

messageListener.listen();

module.exports = server;