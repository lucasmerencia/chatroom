exports.up = function(knex) {
    return knex.schema.createTable('users', function (table) {
        table.string('id', 36).primary();
        table.string('username').notNullable().unique();
        table.string('encrypted_password').notNullable();
        table.timestamp('created_at').notNullable();
        table.timestamp('updated_at').notNullable();
        table.timestamp('deleted_at')
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('users');
};