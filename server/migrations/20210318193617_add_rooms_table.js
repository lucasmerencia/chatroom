exports.up = function(knex) {
    return knex.schema.createTable('rooms', function (table) {
        table.string('id', 36).primary();
        table.string('name').notNullable().unique();
        table.string('created_by', 36).references('id').inTable('users').notNullable();
        table.timestamp('created_at').notNullable();
        table.timestamp('updated_at').notNullable();
        table.timestamp('deleted_at')
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('rooms');
};