const knex = require('knex');

const database = {
  development: {
    client: 'pg',
    connection: process.env.DATABASE_URL || 'postgresql://postgres:postgres@localhost:5432/chatroom',
    afterCreate: function(connection, callback) {
      connection.query('SET timezone="UTC";', function(err) {
          callback(err, connection);
      });
    },
    pool: { 
      min: 1, 
      max: 10,
    }
  },

  test: {
    client: 'sqlite3',
    connection: ':memory:',
    useNullAsDefault: true
  },

  production: {
    client: 'pg',
    connection: process.env.DATABASE_URL,
    afterCreate: function(connection, callback) {
      connection.query('SET timezone="UTC";', function(err) {
          callback(err, connection);
      });
    },
    pool: { 
      min: 1, 
      max: 10,
    }
  },
};

module.exports = database;