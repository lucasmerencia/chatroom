# Chat Room

## Docker Compose

If you have docker composer installed, you may run the project by that:

- Access the project folder and run: `docker-compose up`
- To rebuild the application use the flag *--build*: `docker-compose up --build`

If you don't have docker composer installed, and don't want to, follow the configuration below.

## Configuration

Follow these configurations if you want to setup the project in your environment.

Node version: v14.15.1
NPM version: 6.14.8

## Dependencies
- Postgresql
- Redis

### Building and running server:
- Access the server folder;
- Run: `npm install`
- Set the environment variables if needed.
- Run the database setup: `npx knex migrate:latest` (remeber to set the env var for DATABASE_URL)
- Run in development mode: `npm start dev`

### Running the test suite and code coverage
- Run for testing `npm run test`
- Run for testing and coverage `npm run coverage`

### Building web:
- Access the web folder;
- run: `npm install`
- If you have changed the server http port, set the environment variable `REACT_APP_API_URL`;
- Run in development monde: `npm start dev`

## Server Environment Variables:
- PORT: The port to listen http calls, default `3030`.
- NODE_ENV: default `developmente`
- SECRET: App sercret key for encrypt user password, default: `app-secret`
- JWT_SECRET: A secret key for encrypt and verify JWT tokens, default: `jwt-secret`
- DATABASE_URL: a postgresql database url, default: `postgresql://postgres:postgres@localhost:5432/chatroom`
- REDIS_URL: a redis database url, default: `redis://localhost:6379`


## Web App Environment Variables for Building
- PORT: The port to serve web app pages.
- REACT_APP_API_URL: the base path of the server application, default: `http://localhost:3030`